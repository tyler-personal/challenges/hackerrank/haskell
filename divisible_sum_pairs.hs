{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}
-- https://www.hackerrank.com/challenges/divisible-sum-pairs/submissions/code/107190571
import Data.List
import System.Environment
import System.IO

ar = [1, 3, 2, 6, 1, 2]

pairs [] = []
pairs (x:xs) = [(x, y) | y <- xs] ++ pairs xs

divisibleSumPairs k = length . filter sumDivisibleByK . pairs
  where sumDivisibleByK (x,y) = ((x + y) `mod` k) == 0

readMultipleLinesAsStringArray 0 = return []
readMultipleLinesAsStringArray n = do
    line <- getLine
    rest <- readMultipleLinesAsStringArray(n - 1)
    return (line : rest)

main = do
    stdout <- getEnv "OUTPUT_PATH"
    fptr <- openFile stdout WriteMode

    nkTemp <- getLine
    let nk = words nkTemp

    let _n = read (head nk) :: Int
    let k = read (nk !! 1) :: Int

    arTemp <- getLine
    let ar = Data.List.map (read :: String -> Int) . words $ arTemp

    let result = divisibleSumPairs k ar
    hPutStrLn fptr $ show result

    hFlush fptr
    hClose fptr
