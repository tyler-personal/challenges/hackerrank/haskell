{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}

module Main where

import Control.Monad
import Data.Array
import Data.Bits
import Data.List
import Data.List.Split
import Debug.Trace
import System.Environment
import System.IO
import System.IO.Unsafe

(|>) x f = f x

-- Incomplete
{-@ perm :: {xs:[a] | (length xs) > 0} -> Int @-}
perm xs = tak

  
nonDivisibleSubset k s = "yeet" -- |>
  -- filter (\(x, y) -> (x + y) `mod` k) |>
 

readMultipleLinesAsStringArray 0 = return []
readMultipleLinesAsStringArray n = do
    line <- getLine
    rest <- readMultipleLinesAsStringArray(n - 1)
    return (line : rest)

main = do
    stdout <- getEnv "OUTPUT_PATH"
    fptr <- openFile stdout WriteMode

    nkTemp <- getLine
    let nk = words nkTemp

    let n = read (nk !! 0) :: Int
    let k = read (nk !! 1) :: Int

    sTemp <- getLine
    let s = Data.List.map (read :: String -> Int) . words $ sTemp

    let result = nonDivisibleSubset k s
    hPutStrLn fptr $ show result

    hFlush fptr
    hClose fptr

