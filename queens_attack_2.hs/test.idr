main : IO ()
main = print 4

f : Int -> Int
f x = x + 4

test1 : (f 4) = 8
test1 = Refl

test2 : (f 5) = 7
test2 = Refl
