from enum import Enum, auto
from dataclasses import dataclass
from typing import List, Tuple
from itertools import takewhile

Position = Tuple[int, int]

class Direction(Enum):
    UP         = auto()
    UP_LEFT    = auto()
    LEFT       = auto()
    DOWN_LEFT  = auto()
    DOWN       = auto()
    DOWN_RIGHT = auto()
    RIGHT      = auto()
    UP_RIGHT   = auto()

@dataclass
class Board:
    row: int
    col: int
    max_row: int
    max_col: int
    obstacles: List[Position]

def possible_moves(board: Board) -> List[Position]:
    return [move
            for direction in list(Direction)
            for move in moves_in_direction(board, direction)]

def moves_in_direction(board: Board, direction: Direction) -> List[Position]:
    positions: List[Position] = []
    up    = list(range(board.row, 0, -1))
    left  = list(range(board.col, 0, -1))
    down  = list(range(board.row, board.max_row + 1))
    right = list(range(board.col, board.max_col + 1))

    if direction == Direction.UP:
        positions = [(row, board.col) for row in up]
    elif direction == Direction.DOWN:
        positions = [(row, board.col) for row in down]
    elif direction == Direction.LEFT:
        positions = [(board.row, col) for col in left]
    elif direction == Direction.RIGHT:
        positions = [(board.row, col) for col in right]
    elif direction == Direction.UP_LEFT:
        positions = list(zip(up, left))
    elif direction == Direction.UP_RIGHT:
        positions = list(zip(up, right))
    elif direction == Direction.DOWN_LEFT:
        positions = list(zip(down, left))
    elif direction == Direction.DOWN_RIGHT:
        positions = list(zip(down, right))
    else:
        raise Exception("Some direction was not handled")

    return list(takewhile(
            lambda pos: pos not in board.obstacles,
            [p for p in positions if p != (board.row, board.col)]))

def to_nums(s: str) -> List[int]:
    return [int(num) for num in s.split(" ")]

def to_position(s: str) -> Position:
    row, col = to_nums(s)
    return row, col

if __name__ == '__main__':
    size, obstacle_count = to_nums(input())
    row, col = to_nums(input())
    obstacles = [to_position(input()) for _ in range(1, obstacle_count)]

    board = Board(row, col, size, size, obstacles)
    moves_count = len(possible_moves(board))

    print(moves_count)
