module Three where

import Control.Lens
import Control.Monad
import System.Environment

main :: IO ()
main = do
--  file <- getEnv "OUTPUT_PATH"
  [size, obstacleCount] <- getNums
  [row, col] <- getNums
  obstacles <- mapM (\_ -> getNums <&> \[x,y] -> (y,x)) [1..obstacleCount]



  pure ()
--  writeFile file $ show count

getNums :: IO [Int]
getNums = map read . words <$> getLine
