{-# LANGUAGE RecordWildCards, TupleSections #-}
import Prelude hiding (Left, Right)
import Control.Arrow

type Position = (Int, Int)

data Direction
  = Up    | UpLeft
  | Left  | DownLeft
  | Down  | DownRight
  | Right | UpRight
  deriving Enum

data Board = Board
  { row       :: Int, col      :: Int
  , maxRow    :: Int, maxCol   :: Int
  , obstacles :: [Position]
  }

possibleMoves :: Board -> [Position]
possibleMoves board = [Up ..] >>= movesInDirection board

movesInDirection :: Board -> Direction -> [Position]
movesInDirection Board{..} direction =
  takeWhile (`notElem` obstacles) . filter (/= (row, col)) $
    case direction of
      Up    -> map (,col) up
      Down  -> map (,col) down
      Left  -> map (row,) left
      Right -> map (row,) right
      UpLeft    -> zip up left
      UpRight   -> zip up right
      DownLeft  -> zip down left
      DownRight -> zip down right
  where
    up    = [row, row-1..1]
    left  = [col, col-1..1]
    down  = [row..maxRow]
    right = [col..maxCol]

main :: IO ()
main = do
  [size, obstacleCount] <- getLine <&> toNums
  [row, col] <- getLine <&> toNums
  obstacles <- mapM (\_ -> getLine <&> toPosition) [1..obstacleCount]

  let board = Board row col size size obstacles
  let movesCount = length $ possibleMoves board

  print movesCount

toNums :: String -> [Int]
toNums = words >>> map read

toPosition :: String -> Position
toPosition = toNums >>> (\[r,c] -> (r,c))

(<&>) = flip (<$>)
