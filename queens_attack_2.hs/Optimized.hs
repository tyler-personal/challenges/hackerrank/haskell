module Optimized where

import Control.Lens
import Control.Monad
import System.Environment

data Cell = S | X | A deriving (Eq, Show)
type Board = [[Cell]]

next :: (Int, Int) -> (Int, Int) -> Board -> Board
next (_x,_y) (a, b) board = case board ^? ix x . ix y of
  (Just S) -> next (x, y) (a, b) $ board & ix x . ix y .~ A
  _ -> board
  where (x,y) = (_x+a,_y+b)

main :: IO ()
main = do
--  file <- getEnv "OUTPUT_PATH"
  [size, obstacleCount] <- getNums
  [row, col] <- getNums
  obstacles <- mapM (const getNums) [1..obstacleCount]

  let board =
        [[if [y,x] `elem` obstacles then X else S
          | x <- [1..size]]
          | y <- [1..size]]

  let f = next (row-1, col-1)

  let finalBoard = board
        & f (0, 1) & f (0, -1) & f (1, 0) & f (-1, 0)
        & f (1, 1) & f (-1, -1) & f (-1, 1) & f (1, -1)

  forM_ (reverse finalBoard) print
  let count = length . filter (== A) . join $ finalBoard
  print count
--  writeFile file $ show count

getNums :: IO [Int]
getNums = map read . words <$> getLine
