{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields #-}

import Control.Monad
import Data.Bits
import Data.List
import Data.List.Split
import Debug.Trace
import System.Environment
import System.IO
import Control.Lens

data Diagonal = LeftToRight | RightToLeft deriving Eq
data Node = Node { col :: Int, row :: Int, diagonal :: Diagonal, value :: Int }

type MagicSquare = [Node]
type Invalid a = a

cost :: Invalid MagicSquare -> Int
cost s = nodeSum row 2 s

colSum = nodeSum col
rowSum = nodeSum row
diagonalSum = nodeSum diagonal

nodeSum :: Eq a => (Node -> a) -> a -> MagicSquare -> Int
nodeSum f n = sum . map value . filter ((== n) . f)

mkMagicSquare :: [[Int]] -> Invalid MagicSquare
mkMagicSquare = undefined

main :: IO()
main = do
    file <- getEnv "OUTPUT_PATH" >>= (`openFile` WriteMode)
    s <- replicateM 3 getLine <&> map (map read . words)

    hPrint file . cost $ mkMagicSquare s
    hFlush file
    hClose file
