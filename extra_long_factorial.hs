module Main where

factorial n  
  | n < 1 = 1
  | otherwise = n * factorial (n - 1)

main = do
    n <- readLn :: IO Integer
    print $ factorial n
